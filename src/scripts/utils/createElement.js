/**
 * Функция должна добавлять html-элементу root потомка childOrChildren
 * @param {HTMLElement} root -- родительский элемент
 * @param {string[] | string | HTMLElement | HTMLElement[]} childOrChildren
 */
function parseChild(root, childOrChildren) {
  if (typeof childOrChildren === 'string' || childOrChildren instanceof HTMLElement) {
    root.innerHTML += childOrChildren
  } else if (childOrChildren.tag) {
    root.appendChild(createElement(childOrChildren))
  } else {
    console.log(childOrChildren)
    for (let elem of childOrChildren) {
      parseChild(root, elem)
    }
  }
}

/**
 *
 * Функция для валидации переданных параметров для createElement
 * @param {object} params -- параметры createElement
 */
function validateParams(params) {
  if (typeof params != 'object') {
    throw (typeof params)
  }
}

/**
 *
 * @param {object} params -- Параметры для создания HTMLElement:
 *   tag -- html-тэг элемента;
 *   attributes -- html-атрибуты элемента;
 *   child -- потомки/потомок
 * @returns HTMLElement
 */
export function createElement(params) {
  //Нужно вынести валидацию параметров в отдельную функцию
  validateParams(params);
  let tag = params.tag
  let attributes = params.attributes
  let child = params.child

  let newElement = document.createElement(tag)

  if (attributes) {
    for (let [key, value] of Object.entries(attributes)) {
      newElement.setAttribute(key, value)
    }
  }
  //Если в параметрах передан потомок, то мы должны его "распарсить"
  if (child) {
    parseChild(newElement, child);
  }

  return newElement;
}